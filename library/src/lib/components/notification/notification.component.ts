import {Component, Input} from '@angular/core';
import { NotificationService } from '../../services/notification/notification.service';

@Component({
	selector: 'so-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.scss']
})

export class NotificationComponent {
	@Input() type: String = 'primary';
	@Input() message: String = '';
	@Input() displaying: Boolean = true;
	
	constructor(public notificationService: NotificationService) {}
	
	closeAlert() {
		this.notificationService.cleanMessages();
	}
}
