import {NotificationTypes} from '../../actions/notifications/notifications.actions';

export const notificationDefaultState = {
	'type': '',
	'message': '',
	'displaying': false
};

export function notificationReducer(state = notificationDefaultState, action: any) {
	switch (action.type) {
		case NotificationTypes.AddANotification:
			return Object.assign({}, action.payload, {
				displaying: true
			});
		case NotificationTypes.CleanNotification:
			return notificationDefaultState;
		default:
			return state;
	}
}


